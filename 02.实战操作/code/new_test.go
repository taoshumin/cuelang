/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package code

import (
	"cuelang.org/go/cue"
	"cuelang.org/go/cue/ast"
	"cuelang.org/go/cue/ast/astutil"
	"cuelang.org/go/encoding/gocode/gocodec"
	"cuelang.org/go/encoding/openapi"
	"cuelang.org/go/pkg/encoding/json"
	"testing"
)

func TestNewTable_Validate(t *testing.T) {
	table := &NewTable{
		Seats: 12, // >=8 & <=10
	}

	err := table.Validate()
	if err != nil {
		t.Error(err)
	}
}

// Go代码生成cue
func TestGen(t *testing.T) {
	r := &cue.Runtime{}
	codec := gocodec.New(r, nil)

	table, err := codec.ExtractType(&NewTable{})
	if err != nil {
		t.Error(err)
	}

	f, err := astutil.ToFile(ast.NewStruct(ast.NewIdent("#Table"), table.Syntax().(ast.Expr)))
	if err != nil {
		t.Error(err)
	}
	inst, err := r.CompileFile(f)
	if err != nil {
		t.Error(err)
	}

	openApi, err := openapi.Gen(inst, nil)
	if err != nil {
		t.Error(err)
	}

	bs, err := json.Indent(openApi, "", " ")
	if err != nil {
		t.Error(err)
	}
	t.Log(bs)
}
