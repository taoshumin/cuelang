# cuelang

很重要：
- [cue上下文-包含example示例](https://cuetorials.com/zh/go-api/basics/context/)
- [cue在线转换](https://cuelang.org/play/?id=qhYIy7ED_zn#cue@export@cue)
- [简单入门示例](https://www.shikanon.com/2021/%E7%BC%96%E7%A8%8B%E8%AF%AD%E8%A8%80/cue%E8%AF%AD%E6%B3%95/)

**思想：Schema -> 约束 -> 数据**

## 安装

```shell
go get -u cuelang.org/go/cmd/cue
```

## &

约束与数值使用&字符进行连接时，会将值进行校验。

## 转换

- 语法检查： cue vet type.cue
- 格式化： cue fmt type.cue
- 渲染结构： cue eval type.cue
- 指定渲染： cue eval -e b type.cue
  - -e ： 指定渲染参数
- 转化成 json：cue export json.cue --out json
- 转化成 yaml： cue export json.cue --out yaml
- 转化成 text ：cue export json.cue --out text
- 输出到文件： cue export json.cue --out json --outfile json.cue.json

## 导入

```shell
import
import 表示将其他外部资源转换为 CUE 代码

cue import 将各种格式数据转为 CUE
cue get go 使用 Go 类型生成 CUE
查看 first-steps/import-configuration 以获取更多信息。
just test:
cue get go k8s.io/api/extensions/v1beta1 -v
```


## cue mod

```shell
cue mod init
cue get go ...
```
