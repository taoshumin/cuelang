// cue类型介绍
// 浮点型
a: 1.5
a: float

// 整形
b: 1
b: int

// 数组
d: [1, 2, 3]

// 结构体
g: {
	h: "abc"
}

// 字符串类型
e: string

// null
j: null

// bool
ok: bool

#Language: {
	tag:  string
	name: string
}

// 数组对象 [...string]
languages: [...#Language]

// 运算符 | int类型或string类型
m: int | string

// 运算符 &
p:

// 默认值 * 类型是int，默认为1
n: *1 | int

// 可选类型 ?
o?: int

#my:{
	x?: string
	y?: int
}

parameter: {
    name: string
    image: string
    config?: [...#Config]
}

output: {
    ...
    spec: {
        containers: [{
            name:  parameter.name
            image: parameter.image
            if parameter.config != _|_ {  //  if _variable_！= _ | _ : 如果某些字段不存在
                config: parameter.config
            }
        }]
    }
    ...
}

// 格式化： cue fmt type.cue
