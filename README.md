# [Cue 主要命令](https://www.jianshu.com/p/230a1d371433)

- 安装:

```shell
go install cuelang.org/go/cmd/cue@latest
```

- 打印

```cue
cue def
```

- 校验 `cue vet`

```cue
cue eval 
```

- 导出 `CUE 为其他工具生成 Yaml 和 JSON`

```cue
cue export
```

- 各种数据格式转换为 CUE

```cue
cue import
```

- 从 Go 类型生成 CUE
```cue
cue get go
```

# Cue [基础语法](https://cuetorials.com/zh/overview/scope-and-visibility/)

#### 定义结构 `#mydef`

```cue
// 关闭结构
#Album: {
	artist: string
	title:  string
	year:   int
	// ... ：关闭结构，Private和Public区别
	// 必须在末尾
	... 
}
```

#### 连词 `&`

- 字段、规则和数据结合。

```cue
val: #Def1 & #Def2
val: {foo: "bar", ans: 42}

#Def1: {
	foo: string
	ans: int
}

#Def2: {
	foo: =~"[a-z]+"
	ans: >0
}
```

##### 默认值可选类型

```cue
s: {
	// 设置默认值
	hello: string | *"world" | "apple"
	// 设置可选类型
	count?: int
}
```

#### 定义数组

```cue
#Meta: {
	// 定义类型和校验规则
	version: string & =~"^v[0-9]+\\.[0-9]+\\.[0-9]+$"
	// 定义数组
	labels: [...string]
}
```

#### 数据结构

```cue
N:   null
B:   bool
S:   string
By:  bytes  // 字节
Num: number // 浮点型或整性
Int: int    // 整性
List: [...] // 数组
Struct: {...} //结构体

// “ _”: 任意类型
// "_|_": 错误（error）
```

#### String类型

```cue
str:    "hello world"
smile:  "\U0001F60A"
quoted: "you can \"quote by escaping \\ \""
multiline: """
	hello world
	a "quoted string in a string"
	down under
	   - some author
	"""
```

#### 约束

```cue
// 定义结构
#schema: {
	name: string
	ans:  string
	num:  int | *42
}
// 引用结构 {"elems":{"AA":{"name":"AA"}}}
elems: [Name=_]: #schema & {name: Name}
// 赋值数据
elems: {
	one: {
		ans: "solo"
		num: 1
	}
	two: {
		ans: "life"
	}
}
// 赋值数据
elems: other: {ans: "id", num: 23}
```

#### 表达式

- 正则表达式 `=~`

```cue
a: "hello world" & =~"^hello [a-z]+$"
```

- 插值
```cue
container: {
	repo:    "docker.io/cuelang"
	image:   "cue"
	version: "v0.3.0"
	full:    "\(repo)/\(image):\(version)"
}
```

- 便利

```cue
nums: [1, 2, 3, 4, 5, 6]
sqrd: [ for _, n in nums {n * n}]
even: [ for _, n in nums if mod(n, 2) == 0 {n}]
```

- 示例

```cue
apps: ["nginx", "express", "postgres"]
// 定义map
#labels: [string]: string
stack: {
	for i, app in apps {
		"\(app)": {
			name:   app
			labels: #labels & {
				app:  "foo"
				tier: "\(i)"
			}
		}
	}
}
```

- 条件判断 `没有else语句，你必须有两个相反的条件`

```cue
app: {
	name: string
	tech: string
	mem:  int

	if tech == "react" {
		tier: "frontend"
	}
}
```

# 隐藏字段

- 下划线开头的真实标签
```cue
A: {
	_hidden: "a hidden field"
	isshown: "I can be seen"
	hidrefd: _hidden + " sort of?"
}
-------------------
A: {
	isshown: "I can be seen"
	hidrefd: "a hidden field sort of?"
}
```

# Tag (标签)


# Go处理

- 从string中获取 Value

```cue
ctx := cuecontext.New()
v := ctx.CompileString(userConfig)
```

- 从[]byte字节从获取 value 

```cue
ctx := cuecontext.New()
v := ctx.CompileBytes(userConfig)
```

- 从文件目录获取

```cue
ctx := cuecontext.New()
binst := load.Instances([]string{"definitions.cue", "testdata/sample.cue"}, nil)
insts, err := ctx.BuildInstances(binst)
```

# 扩展
- 或 ： `|`
- 任意值: `-`
- 空对象： `表示错误用_|_`
- 拼接字符串： ` \(变量) `
- 可选参数： `？`


# 相关知识

#### 一、[Cue 基础知识](./01.基础知识/README.md)
#### 二、[Cue 基础语法加强](https://www.shikanon.com/2021/%E7%BC%96%E7%A8%8B%E8%AF%AD%E8%A8%80/cue%E8%AF%AD%E6%B3%95/)
#### 三、[Cue 命令行](./03.命令行介绍/README.md)
