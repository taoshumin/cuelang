/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package code

import (
	"cuelang.org/go/encoding/gocode/gocodec"
)

type Restaturant struct {
	Cuisine string      `json:"cuisine"`
	Tables  []*NewTable `json:"tables,omitempty"`
}

type NewTable struct {
	Seats int  `json:"seats,omitempty" cue:">=0 & <= 10"`
	View  bool `json:"view,omitempty"`
}

func (t NewTable) Validate() error {
	return gocodec.Validate(t)
}

// generates files in the main module locally
// Copy https://cuelang.org/docs/integrations/go/
