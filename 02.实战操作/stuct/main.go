/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"cuelang.org/go/cue"
	"cuelang.org/go/cue/cuecontext"
	"cuelang.org/go/encoding/gocode/gocodec"
	"cuelang.org/go/pkg/encoding/json"
	_ "embed"
	"fmt"
)

//go:embed user.cue
var userConfig string

type Users struct {
	UserID   int    `json:"user_id"`
	UserName string `json:"user_name"`
}

func main() {
	example2()
	ctx := cuecontext.New()
	// 校验cue格式
	v := ctx.CompileString(userConfig, []cue.BuildOption{}...)
	// 获取Users的校验规则
	cuValue := v.LookupPath(cue.ParsePath("Users"))
	// 创建用户
	user := &Users{
		UserID:   120,
		UserName: "te1",
	}

	uservv := ctx.Encode(user)
	data, err := json.Marshal(uservv)
	if err != nil {
		panic(err)
	}

	ok, err := json.Validate([]byte(data), cuValue)
	if err != nil {
		panic(err)
	}
	fmt.Println(ok)
}

type ab struct {
	A int
	B int
}

// 将cue转换成结构体
func example() {
	var x ab

	ctx := cuecontext.New()
	i := ctx.CompileString(`{A: 2, B: 4}`)
	_ = i.Value().Decode(&x)
	fmt.Println(x)

	i = ctx.CompileString(`{B: "foo"}`)
	_ = i.Value().Decode(&x)
	fmt.Println(x)
}

// 将Go结构体转成cue
// 从Go类型中提取cue ExtractType
func example2() {
	user := &Users{
		UserID:   120,
		UserName: "te1",
	}

	ctx := cuecontext.New()
	// 将go的值转换成cue
	i := ctx.Encode(user)
	fmt.Println(i)

	codec := gocodec.New((*cue.Runtime)(ctx), nil)
	// 从Go的type从提取value的类型，string
	v1, err := codec.ExtractType(user)
	if err != nil {
		panic(err)
	}
	fmt.Println(v1)
	v2 := ctx.CompileString(userConfig, []cue.BuildOption{}...)
	cuValue := v2.LookupPath(cue.ParsePath("Users"))
	fmt.Println(v2)
	// 检查v1是否满足v2的定义
	err = codec.Validate(cuValue, i)
	if err != nil {
		panic(err)
	}
}

func example3() {
	var (
		name        = "string list incompatible lengths"
		value       = []string{"a", "b", "c"}
		constraints = `4*[string]`
	)

	ctx := cuecontext.New()
	codec := gocodec.New((*cue.Runtime)(ctx), nil)

	v, err := codec.ExtractType(value)
	if err != nil {
		panic(err)
	}

	// 添加约束和值
	insts := ctx.CompileString(constraints, cue.Filename(name))
	if err != nil {
		panic(err)
	}
	w1 := insts.Value()
	// 合并校验规则和值
	v = v.Unify(w1)

	err = codec.Validate(v, value)
	if err != nil {
		panic(err)
	}
}
