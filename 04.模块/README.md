# Mod

### 使用流程

创建:

```shell
cue mod init gitlab.gridsum.com/goboot
```

查看:

```shell
╰─± tree
.
├── README.md
└── cue.mod
    ├── module.cue  # 注释 module: "gitlab.gridsum.com/goboot"
    ├── pkg
    └── usr
```

- pkg : 一个导入的外部 CUE 包，
- gen：从外部定义生成的 CUE，例如 protobuf 或 Go，
- usr：上述两个目录的用户定义约束。

生成package:

```shell
cue import ./... -p kube
```