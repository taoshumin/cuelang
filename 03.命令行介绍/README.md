# Cue 命令行介绍

### import

表示将其他外部资源转换为cue代码。

- cue import: 将各种格式数据转为cue
- cue get to: 使用 go类型生成cue

### 主要命令

- cue def: 将配置文件打印为同一个文件。（运行代码前查看）
- cue eval: 计算结果，验证并打印配置。
- cue export: 根据参数计算并输出配置。（cue代码生成yaml或json）
- cue vet: 根据定义的结构验证数据。
  - `-c`: 确认具体数值
  - `-d`: 指定要验证的结构

### 演示示例

将cue生成yaml:

```shell
cue export example1.cue
```

将yaml文件生成cue: （deployment.cue）

```shell
cue import deployment.yaml
```

将go文件生成cue:

```shell
cue get go . --local
```

将json生成cue文件: (example2.cue)

```shell
cue import -R example2.json
```