/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"log"

	"cuelang.org/go/cue"
	"cuelang.org/go/cue/cuecontext"
	"cuelang.org/go/cue/load"
)

// https://github.com/cue-lang/cue/discussions/1030
func main() {
	ctx := cuecontext.New()

	binst := load.Instances([]string{"definitions.cue", "testdata/sample.cue"}, nil)
	insts, err := ctx.BuildInstances(binst)
	if err != nil {
		log.Fatal(err)
	}
	if len(insts) != 1 {
		log.Fatal("more than one instance")
	}
	v := insts[0]
	opts := []cue.Option{
		cue.Attributes(true),
		cue.Definitions(true),
		cue.Hidden(true),
	}
	err = v.Validate(opts...)
	if err != nil {
		log.Fatal(err)
	}
}

/*
	func main() {
	ctx := cuecontext.New()

	binst := load.Instances([]string{"definitions.cue"}, nil)
	insts, err := ctx.BuildInstances(binst)
	if err != nil {
		log.Fatal(err)
	}
	if len(insts) != 1 {
		log.Fatal("more than one instance")
	}
	v := insts[0]
	b, err := ioutil.ReadFile("testdata/bad.json")
	if err != nil {
		log.Fatal(err)
	}
	err = json.Validate(b, v)
	if err == nil {
		log.Fatal(err)

	}
}
*/
