# Cue 实战

- [https://www.youtube.com/watch?v=IRNluM2B4p8](https://www.youtube.com/watch?v=IRNluM2B4p8)
- [https://cuelang.org/docs/integrations/go/](https://cuelang.org/docs/integrations/go/)

go代码生成cue文件: [bug](https://github.com/cue-lang/cue/issues/848)
```cue
cue get go . --local
cue get go . --local --ssafile=ssa.cue
cue def .
```

打印openapi和yaml文件格式：

```cue
cue def .
cue def . --out openapi+yaml
```
