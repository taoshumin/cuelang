package event

// Definition of the envelop
#Envelop: {
	subject:          =~"^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$" // a uuid
	source:           =~"^net.mycompany.+"
	type:             =~"^net.mycompany.+:[cud]" // c : create u : update d : delete
	version:          =~"^v.*"                   // TODO put a proper version check
	dataContentType?: string
}