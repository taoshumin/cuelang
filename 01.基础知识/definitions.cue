package sample1

import (
	"test.io/cuelang/event:event"
)

#Schema: {
	envelop: event.#Envelop & {
		source:  "net.mycompany.sample1"
		version: "v1.0.0"
	}
	data: {
		// size is the size in centimeters
		size: int & <250
	}
}